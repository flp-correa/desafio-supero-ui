import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { ConfirmationService } from 'primeng/api';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastyModule } from 'ng2-toasty';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

import { NavbarComponent } from './navbar/navbar.component';
import { ErrorHandlerService } from './error-handler.service';
import { TarefaService } from '../tarefas/tarefa.service';
import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    ToastyModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    ConfirmDialogModule,
  ],
  declarations: [ NavbarComponent, PaginaNaoEncontradaComponent ],
  exports: [ 
    NavbarComponent,
    ToastyModule,
    ConfirmDialogModule,
    Ng4LoadingSpinnerModule
  ],
  providers: [
    ErrorHandlerService,
    TarefaService,
    ConfirmationService,
    Title
  ]
})
export class CoreModule { }
