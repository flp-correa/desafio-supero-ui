export class Tarefa {
    codigo: number;
    titulo: string;
    descricao: string;
    dataCriacao: Date;
    status: boolean;
}