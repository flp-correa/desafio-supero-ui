import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TarefaCadastroComponent } from './tarefa-cadastro/tarefa-cadastro.component';
import { TarefasPesquisaComponent } from './tarefas-pesquisa/tarefas-pesquisa.component';


const routes: Routes = [
    { path: 'tarefas', component: TarefasPesquisaComponent },
    { path: 'tarefas/nova', component: TarefaCadastroComponent },
    { path: 'tarefas/:codigo', component: TarefaCadastroComponent },
  ];

  @NgModule({
    imports: [
      RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
  })
  export class TarefaRoutingModule { }