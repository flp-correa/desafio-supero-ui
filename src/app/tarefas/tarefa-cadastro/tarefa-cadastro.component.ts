import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { ToastyService } from 'ng2-toasty';

import { Tarefa } from '../../core/model';
import { TarefaService } from '../tarefa.service';
import { ErrorHandlerService } from '../../core/error-handler.service';

@Component({
  selector: 'app-tarefa-cadastro',
  templateUrl: './tarefa-cadastro.component.html',
  styleUrls: ['./tarefa-cadastro.component.css']
})
export class TarefaCadastroComponent implements OnInit {

  situacoes = [
    { label: 'Em andamento', value: false },
    { label: 'Concluída', value: true },
  ];

  tarefa = new Tarefa();
  dataHoje = new Date();

  constructor(private tarefaService: TarefaService,
              private toasty: ToastyService,
              private errorHandler: ErrorHandlerService,
              private route: ActivatedRoute,
              private router: Router,
              private title: Title) { }

  ngOnInit() {
    const codigoTarefa = this.route.snapshot.params['codigo'];

    this.title.setTitle('Nova tarefa');

    if (codigoTarefa) {
      this.carregarTarefa(codigoTarefa);
    }
  }

  salvar(form: FormControl) {
    if (this.editando) {
      this.atualizarTarefa();
    } else {
      this.adicionarTarefa();
    }
  }

  nova(form: FormControl) {
    form.reset();

    this.tarefa = new Tarefa();
    this.router.navigate(['/tarefas/nova']);
  }

  get editando() {
    return Boolean(this.tarefa.codigo)
  }

  private adicionarTarefa() {
    this.tarefaService.adicionar(this.tarefa)
      .then(tarefaAdicionada => {
        this.toasty.success('Tarefa adicionada com sucesso!');
        this.router.navigate(['/tarefas', tarefaAdicionada.codigo]);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  private atualizarTarefa() {
    this.tarefaService.atualizar(this.tarefa)
      .then(tarefa => {
        this.tarefa = tarefa;

        this.toasty.success('Tarefa alterada com sucesso!');
        this.atualizarTituloEdicao();
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  private carregarTarefa(codigo: number) {
    this.tarefaService.buscarPorCodigo(codigo)
      .then(tarefa => {
        this.tarefa = tarefa;
        this.atualizarTituloEdicao();
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.router.navigate(['/pagina-nao-encontrada']);
      });
  }

  private atualizarTituloEdicao() {
    this.title.setTitle(`Edição da tarefa: ${this.tarefa.titulo}`);
  }

}
