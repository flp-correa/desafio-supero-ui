import { Component, ViewChild, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { LazyLoadEvent, ConfirmationService } from 'primeng/components/common/api';

import { TarefaService, TarefaFiltro } from '../tarefa.service';
import { ToastyService } from 'ng2-toasty';
import { ErrorHandlerService } from '../../core/error-handler.service';

@Component({
  selector: 'app-tarefas-pesquisa',
  templateUrl: './tarefas-pesquisa.component.html',
  styleUrls: ['./tarefas-pesquisa.component.css']
})
export class TarefasPesquisaComponent implements OnInit {

  totalRegistros = 0;
  filtro = new TarefaFiltro();

  situacoes = [
    { label: 'Em andamento', value: false },
    { label: 'Concluída', value: true },
  ];

  tarefas = [];

  @ViewChild('tabela') grid;

  constructor(private tarefaService: TarefaService,
              private errorHandler: ErrorHandlerService,
              private toasty: ToastyService,
              private confirmation: ConfirmationService,
              private title: Title) {}

  ngOnInit() {
    this.title.setTitle('Pesquisa de tarefas');
  }

  pesquisar(pagina = 0) {
    this.filtro.pagina = pagina;
    this.tarefaService.pesquisar(this.filtro)
      .then(resultado => {
        this.tarefas = resultado.tarefas;
        this.totalRegistros = resultado.total;
      })
      .catch(erro => this.errorHandler.handle(erro)
      );
  }

  excluir(tarefa: any) {
    this.tarefaService.excluir(tarefa.codigo)
      .then(() => {
        this.grid.first = 0;
        this.pesquisar();
        /*if (this.grid.first === 0) {
        } else {
          this.grid.first = 0;
        }*/
        this.toasty.success('Tarefa excluída com sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  alternarStatus(tarefa: any): void {
    const novoStatus = !tarefa.status;

    this.tarefaService.mudarStatus(tarefa.codigo, novoStatus)
      .then(() => {
        const acao = novoStatus ? 'concluída' : 'em andamento';

        tarefa.status = novoStatus;
        this.toasty.success(`Tarefa ${acao}!`);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  confirmarExclusao(tarefa: any) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir?',
      accept: () => {
        this.excluir(tarefa);
      }
    });
  }


  aoMudarPagina(event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisar(pagina);
  }

}
