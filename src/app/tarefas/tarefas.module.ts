import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { CalendarModule } from 'primeng/calendar';
import { SelectButtonModule } from 'primeng/selectbutton';

import { SharedModule } from '../shared/shared.module';
import { TarefaCadastroComponent } from './tarefa-cadastro/tarefa-cadastro.component';
import { TarefasPesquisaComponent } from './tarefas-pesquisa/tarefas-pesquisa.component';
import { TarefaRoutingModule } from './tarefas-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    InputTextModule,
    ButtonModule,
    RadioButtonModule,
    InputTextareaModule,
    TableModule,
    TooltipModule,
    CalendarModule,
    SelectButtonModule,

    SharedModule,
    TarefaRoutingModule
  ],
  declarations: [
    TarefasPesquisaComponent,
    TarefaCadastroComponent
  ],
  exports: []
})
export class TarefasModule { }
