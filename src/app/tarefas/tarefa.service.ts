import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http'

import * as moment from 'moment'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';

import { Tarefa } from '../core/model';

export class TarefaFiltro {
  titulo: string;
  descricao: string;
  dataCriacaoInicio: Date;
  dataCriacaoFim: Date;
  status: boolean;
  pagina = 0;
  itensPorPagina = 5;
}

@Injectable()
export class TarefaService {

  tarefasUrl: string;

  constructor(private http: Http, 
              private spinnerService: Ng4LoadingSpinnerService) {
    this.tarefasUrl = environment.apiUrl.concat('/tarefas');
   }

  pesquisar(filtro: TarefaFiltro): Promise<any> {
    this.spinnerService.show();
    const params = this.adicionaParametro(filtro);
    params.set('page', filtro.pagina.toString());
    params.set('size', filtro.itensPorPagina.toString());

    return this.http.get(`${this.tarefasUrl}?`, { search: params })
      .toPromise()
      .then(response => {
        const reponseJson = response.json();
        const tarefas = reponseJson.content;
        const resultado = {
          tarefas: tarefas,
          total: reponseJson.totalElements
        }
        this.spinnerService.hide();
        return resultado;
      });
  }

  adicionar(tarefa: Tarefa): Promise<Tarefa> {
    this.spinnerService.show();
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.tarefasUrl.concat("/nova"),
      JSON.stringify(tarefa), { headers })
        .toPromise()
        .then(response => {
          this.spinnerService.hide();
          return response.json();
        });
  }

  atualizar(tarefa: Tarefa): Promise<Tarefa> {
    this.spinnerService.show();
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.put(`${this.tarefasUrl}/${tarefa.codigo}`,
        JSON.stringify(tarefa), { headers })
      .toPromise()
      .then(response => {
        const tarefaAlterada = response.json() as Tarefa;

        this.converterStringsParaDatas([tarefaAlterada]);
        this.spinnerService.hide();

        return tarefaAlterada;
      });
  }

  excluir(codigo: number): Promise<void> {
    this.spinnerService.show();
    return this.http.delete(`${this.tarefasUrl}/${codigo}`)
      .toPromise()
      .then(() => this.spinnerService.hide());
  }

  buscarPorCodigo(codigo: number): Promise<Tarefa> {
    this.spinnerService.show();
    const headers = new Headers();

    return this.http.get(`${this.tarefasUrl}/${codigo}`, { headers })
      .toPromise()
      .then(response => {
        const tarefa = response.json() as Tarefa;

        this.converterStringsParaDatas([tarefa]);
        this.spinnerService.hide();
        return tarefa;
      });
  }

  mudarStatus(codigo: number, status: boolean): Promise<void> {
    this.spinnerService.show();
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.put(`${this.tarefasUrl}/${codigo}/status`, status, { headers })
      .toPromise()
      .then(() => this.spinnerService.hide());
  }

  private converterStringsParaDatas(tarefas: Tarefa[]) {
    tarefas.forEach(tarefa => tarefa.dataCriacao = moment(tarefa.dataCriacao, 'YYYY-MM-DD').toDate());
  }


  private adicionaParametro(filtro: TarefaFiltro) {
    const params = new URLSearchParams();
    if (filtro.titulo) {
      params.set('titulo', filtro.titulo);
    }
    if (filtro.descricao) {
      params.set('descricao', filtro.descricao);
    }
    if (filtro.dataCriacaoInicio) {
      params.set('dataCriacaoInicio', moment(filtro.dataCriacaoInicio).format('YYYY-MM-DD'));
    }
    if (filtro.dataCriacaoFim) {
      params.set('dataCriacaoFim', moment(filtro.dataCriacaoFim).format('YYYY-MM-DD'));
    }
    if (filtro.status !== undefined) {
      params.set('status', filtro.status.toString());
    }
    return params;
  }

}
